import os
import pathlib


def gather(hub):
    """
    Gather the version number from salt
    """
    pybin = hub.OPT.pkgr.python
    cdir = pathlib.Path(hub.OPT.pkgr.sdir, "salt")
    hub.tiamat.cmd.run([pybin, "setup.py", "sdist"], cwd=cdir, fail_on_error=True)

    version = None
    for fn in cdir.joinpath("dist").iterdir():
        fn = str(fn)
        version = fn[fn.index("-") + 1 : fn[: fn.rindex(".")].rindex(".")]
    return version
