def gather(hub):
    """
    Gather the version number from the underlying project
    """
    ver = hub.OPT.pkgr.ver
    hub.pkgr.SALT_VERSION = ver
    if ver in hub.pkgr.ver:
        hub.pkgr.SALT_VERSION = getattr(hub, f"pkgr.ver.{ver}.gather")()
        return
