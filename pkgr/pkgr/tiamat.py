import os
import pathlib
import shutil


def __virtual__(hub):
    required_bins = {"tar", "tiamat"}
    for rbin in list(required_bins):
        if shutil.which(rbin) is not None:
            required_bins.remove(rbin)
    if required_bins:
        return (
            False,
            f"The following binaries were not found on the system: {', '.join(required_bins)}",
        )
    return True


def build(hub):
    """
    Build the package using tiamat
    """
    # Copy the required files into salt dir
    # build with tiamat with build.conf file
    sources = os.path.join(hub.OPT.pkgr.sdir, "salt")
    for fn in os.listdir(hub.OPT.pkgr.sources):
        full = os.path.join(hub.OPT.pkgr.sources, fn)
        shutil.copy(full, sources)

    os.chdir(sources)

    hub.tiamat.build.builder(
        name=hub.OPT.tiamat.name,
        requirements=hub.OPT.tiamat.requirements,
        sys_site=hub.OPT.tiamat.system_site,
        exclude=hub.OPT.tiamat.exclude,
        directory=os.path.abspath(sources),
        pyinstaller_version=hub.OPT.tiamat.pyinstaller_version,
        pyinstaller_runtime_tmpdir=hub.OPT.tiamat.pyinstaller_runtime_tmpdir,
        datas=hub.OPT.tiamat.datas,
        build=hub.OPT.tiamat.build,
        pkg=hub.OPT.tiamat.pkg,
        onedir=hub.OPT.tiamat.onedir,
        pyenv=hub.OPT.tiamat.pyenv,
        run=hub.OPT.tiamat.run,
        no_clean=hub.OPT.tiamat.no_clean,
        locale_utf8=hub.OPT.tiamat.locale_utf8,
        dependencies=hub.OPT.tiamat.dependencies,
        release=hub.OPT.tiamat.release,
        pkg_tgt=hub.OPT.tiamat.pkg_tgt,
        pkg_builder=hub.OPT.tiamat.pkg_builder,
        srcdir=hub.OPT.tiamat.srcdir,
        system_copy_in=hub.OPT.tiamat.system_copy_in,
        tgt_version=hub.OPT.tiamat.tgt_version,
        venv_plugin=hub.OPT.tiamat.venv_plugin,
        python_bin=hub.OPT.tiamat.python_bin,
        omit=hub.OPT.tiamat.omit,
        pyinstaller_args=hub.OPT.tiamat.pyinstaller_args,
    )

    # create versioned tarfile
    dist = pathlib.Path("dist").resolve()

    grains_map = {"arch": {"x86_64": "amd64", "aarch64": "arm64", "armv7l": "armv7"}}
    kernel = hub.grains.GRAINS["kernel"].lower()
    arch = hub.grains.GRAINS["cpuarch"].lower()
    tarball_name = f"salt-{hub.pkgr.SALT_VERSION}-{hub.pkgr.PKG_VERSION}-{kernel}-{grains_map['arch'][arch]}.tar.gz"
    hub.tiamat.cmd.run(
        ["tar", "-czvf", tarball_name, "salt"], cwd=str(dist), fail_on_error=True
    )
    artifacts = os.path.join(hub.pkgr.CDIR, "artifacts", hub.pkgr.SALT_VERSION)
    if not os.path.isdir(artifacts):
        hub.log.debug(f"[pkgr] Making Directory: {artifacts}")
        os.makedirs(artifacts)
    shutil.copy(str(dist / tarball_name), artifacts)


def render(hub):
    pass
