import datetime
import os
import pathlib
import shutil
import tarfile

import jinja2


def __virtual__(hub):
    required_bins = {"tar", "cp", "rm", "debuild"}
    for rbin in list(required_bins):
        if shutil.which(rbin) is not None:
            required_bins.remove(rbin)
    if required_bins:
        return (
            False,
            f"The following binaries were not found on the system: {', '.join(required_bins)}",
        )
    return True


def extract_sdist(hub):
    """
    Extract and use the sdist generated
    """
    sdir = pathlib.Path(hub.OPT.pkgr.sdir)
    saltd = sdir / "salt"
    sdist = [e for e in saltd.joinpath("dist").iterdir()][0]

    with tarfile.open(sdist) as fp:
        fp.extractall(path=sdir)

    shutil.rmtree(saltd)

    saltv = [e for e in sdir.iterdir()][0]
    saltv.rename(saltd)


def copytree(hub, src, dst, *args, **kwargs):
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            shutil.copytree(s, d, *args, **kwargs)
        else:
            shutil.copy2(s, d)


def build(hub):
    """
    Build the package!
    """
    # copy sources into the build tree
    # name them by version
    # create source tarballs
    # place the changelog file down with the changes from the jinja template
    # debuild --no-sign -F
    # copy results of build to artifacts folder

    extract_sdist(hub)
    hub.pkgr.deb.copytree(hub.OPT.pkgr.sdir, hub.pkgr.BDIR)

    proj_dir = ""
    relative_proj_dir = ""
    # change to the directory we extracted, bail if there is not exactly 1 directory
    with os.scandir(hub.pkgr.BDIR) as it:
        dirs = []
        for entry in it:
            if entry.is_dir():
                dirs.append(entry)
        if len(dirs) != 1:
            raise ValueError("Multiple directories found in build dir")
        else:
            proj_dir = os.path.join(hub.pkgr.BDIR, dirs[0].name)
            relative_proj_dir = dirs[0].name
    salt_version = hub.pkgr.SALT_VERSION.replace("rc", "~rc")
    versioned_proj_dir = f"{proj_dir}_{salt_version}+ds"
    relative_versioned_proj_dir = f"{relative_proj_dir}_{salt_version}+ds"
    hub.log.debug(f"[pkgr] Renaming {proj_dir} to {versioned_proj_dir}")
    os.rename(proj_dir, versioned_proj_dir)

    hub.tiamat.cmd.run(
        [
            "tar",
            "-acvf",
            f"{versioned_proj_dir}.orig.tar.xz",
            "-C",
            hub.pkgr.BDIR,
            relative_versioned_proj_dir,
        ],
        fail_on_error=True,
    )
    shutil.copytree(
        hub.OPT.pkgr.debian_dir,
        pathlib.Path(versioned_proj_dir) / hub.OPT.pkgr.debian_dir.rsplit("/")[-1],
    )

    changelog = os.path.join(versioned_proj_dir, "debian/changelog")
    hub.log.debug(f"[pkgr] Writing changelog to {changelog}:")
    hub.log.debug(hub.pkgr.CHANGELOG)
    with open(changelog, "w+") as wfh:
        wfh.write(hub.pkgr.CHANGELOG)

    hub.tiamat.cmd.run(
        [
            "tar",
            "-acvf",
            f"{versioned_proj_dir}-1.debian.tar.xz",
            "-C",
            versioned_proj_dir,
            "debian",
        ],
        fail_on_error=True,
    )

    hub.tiamat.cmd.run(
        ["debuild", "-us", "-uc", "-F"], cwd=versioned_proj_dir, fail_on_error=True
    )

    artifacts = os.path.join(hub.pkgr.CDIR, "artifacts")
    if not os.path.isdir(artifacts):
        hub.log.debug(f"[pkgr] Making Directory: {artifacts}")
        os.mkdir(artifacts)
    with os.scandir(hub.pkgr.BDIR) as it:
        for entry in it:
            if entry.is_file():
                hub.log.debug(f"[pkgr] Moving {entry.name} to {artifacts}")
                shutil.copy(os.path.join(hub.pkgr.BDIR, entry.name), artifacts)


def render(hub):
    """
    Render the changelog file
    """
    opts = dict(hub.OPT.pkgr)
    salt_version = hub.pkgr.SALT_VERSION.replace("rc", "~rc")
    opts["version"] = salt_version
    opts["pkg_version"] = hub.pkgr.PKG_VERSION
    utcnow = datetime.datetime.now(tz=datetime.timezone.utc)
    opts["changelog_date"] = utcnow.strftime("%a, %d %b %Y %H:%M:%S %z")
    with open(os.path.join(hub.pkgr.CDIR, hub.OPT.pkgr.debian_dir, "changelog")) as rfh:
        data = rfh.read()
    template = jinja2.Template(data)
    hub.pkgr.CHANGELOG = template.render(**opts)
