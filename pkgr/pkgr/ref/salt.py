import re

# This is basically a copy of the regexes from salt/version.py with added
# support for packaging version
GIT_SHA_REGEX = r"(?P<sha>g?[a-f0-9]{7,40})"
GIT_DESCRIBE_REGEX = (
    r"(?:[^\d]+)?(?P<major>[\d]{1,4})"
    r"(?:\.(?P<minor>[\d]{1,2}))?"
    r"(?:\.(?P<bugfix>[\d]{0,2}))?"
    r"(?:\.(?P<mbugfix>[\d]{0,2}))?"
    r"(?:(?P<pre_type>rc|a|b|alpha|beta|nb)(?P<pre_num>[\d]{1}))?"
    r"(?:(?:.*)-(?P<noc>(?:[\d]+|n/a))-" + GIT_SHA_REGEX + r")?"
)
PACKAGING_REGEX = re.compile(
    r"(?P<salt_version>" + GIT_DESCRIBE_REGEX + r")" r"(?:-(?P<pkg_version>[\d]{1,2}))?"
)
GIT_DESCRIBE_REGEX = re.compile(GIT_DESCRIBE_REGEX)
GIT_SHA_REGEX = re.compile(r"^" + GIT_SHA_REGEX)


def gather(hub):
    """
    Gather the checkout ``ref`` based on the salt versioning
    """
    match = PACKAGING_REGEX.match(hub.OPT.pkgr.ref)
    if match:
        return match.group("salt_version"), match.group("pkg_version")
    return None, None
