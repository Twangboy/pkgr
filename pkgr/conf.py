import sys
import tempfile

CLI_CONFIG = {
    "spec": {},
    "sources": {},
    "git": {},
    "ref": {},
    "pkg_version": {},
    "system": {},
    "config": {"options": ["-c"], "source": "tiamat"},
    "sdir": {},
}
CONFIG = {
    "spec": {
        "default": None,
        "help": "The location of the spec file to use when using the rpm system",
    },
    "sources": {
        "default": "",
        "help": "a directory containing extra sources files when using the rpm system",
    },
    "debian_dir": {
        "default": "",
        "help": "the debian directory to copy into the project when using the deb system",
    },
    "git": {
        "default": None,
        "help": "A git location to gather the project from",
    },
    "ref": {
        "default": None,
        "help": "The name of the git branch, commit or tag to checkout",
    },
    "system": {
        "default": "tiamat",
        "help": "Choose what type of system to build to, rpm or deb",
    },
    "ver": {
        "default": "salt",
        "help": (
            "Choose what version gathering system to use to dynamically determine the "
            "version number to apply to the package. If instead you wish to statically "
            "set the version number simply pass the desired version number"
        ),
    },
    "pkg_version": {
        "default": None,
        "help": (
            "The package version to use(this is not the project version). Defaults to: 1"
        ),
    },
    "python": {
        "default": sys.executable,
        "help": "The python binary to use.",
    },
    "config": {
        "default": "",
        "help": "The location of the configuration file",
    },
    "sdir": {
        "default": tempfile.mkdtemp(),
        "help": "The source directory where a git clone of salt exists. "
        "By default it will be a temporary directory",
    },
}
SUBCOMMANDS = {}
DYNE = {
    "pkgr": ["pkgr"],
}
