def test_spec(mock_hub, hub, tmp_path):
    # set test options
    mock_hub.OPT.pkgr = {"system": "rpm"}
    mock_hub.pkgr.SALT_VERSION = "4000"
    mock_hub.pkgr.PKG_VERSION = "99"
    spec = tmp_path / "salt.spec"
    spec.write_text("{{ version }}-{{pkg_version}}")
    mock_hub.OPT.pkgr.spec = spec
    # Hook into the real render method
    mock_hub.pkgr.rpm.render = hub.pkgr.rpm.render
    mock_hub.pkgr.rpm.render()
    assert mock_hub.pkgr.SPEC == "4000-99"
