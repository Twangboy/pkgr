import pathlib
import subprocess
from unittest import mock

from dict_tools import data


def test_build(mock_hub, hub):
    """
    Test correct paths and files are created
    when running hub.pkgr.tiamat.build
    """
    # set test options
    mock_hub.OPT.pkgr.system = "tiamat"
    mock_hub.OPT.pkgr.sources = mock_hub.pkgr.CDIR.join("sources").ensure(dir=True)
    mock_hub.pkgr.SALT_VERSION = "4000"
    mock_hub.pkgr.PKG_VERSION = "99"
    mock_hub.OPT.tiamat = mock.MagicMock()

    # create test files
    tarball_name = f"{mock_hub.pkgr.proj_name}-{mock_hub.pkgr.SALT_VERSION}-{mock_hub.pkgr.PKG_VERSION}-linux-amd64.tar.gz"
    tar_file = mock_hub.OPT.pkgr.sdir.join("salt", "dist", tarball_name)
    tar_file.write("", ensure=True)
    run_file = mock_hub.pkgr.CDIR.join("sources", "run.py")
    run_file.write("", ensure=True)

    mock_hub.tiamat.cmd.run = hub.tiamat.cmd.run
    mock_hub.pkgr.tiamat.build = hub.pkgr.tiamat.build
    mock_hub.pkgr.tiamat.build()
    call_args = mock_hub.tiamat.build.builder.call_args_list[0][1]
    assert call_args["directory"] == mock_hub.OPT.pkgr.sdir.join("salt")
    assert pathlib.Path(
        mock_hub.pkgr.CDIR.join("artifacts", mock_hub.pkgr.SALT_VERSION, tarball_name)
    ).exists()
