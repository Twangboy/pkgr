import pathlib
import tarfile


def test_sdist(mock_hub, hub):
    """
    Test correct paths and files are created
    when running hub.pkgr.rpm.build
    """
    # set test options
    mock_hub.OPT.pkgr.system = "deb"

    sdir = pathlib.Path(mock_hub.OPT.pkgr.sdir)
    dist_dir = sdir / "salt" / "dist"
    dist_dir.mkdir(parents=True)
    test_file = dist_dir / "test_file"
    with open(test_file, "w") as test_fp:
        test_fp.write("test")
    tar_file = (
        dist_dir / f"{mock_hub.pkgr.proj_name}-{mock_hub.pkgr.SALT_VERSION}.tar.gz"
    )

    with tarfile.open(tar_file, "w:gz") as tar_fp:
        tar_fp.add(test_file, arcname="salt-3000/test_file")
    test_file.unlink()

    mock_hub.pkgr.deb.extract_sdist = hub.pkgr.deb.extract_sdist
    mock_hub.pkgr.deb.extract_sdist()
    assert pathlib.Path(sdir / "salt" / "test_file").is_file()

    contents = [e for e in sdir.iterdir()]
    assert contents[0].name == "salt"
    assert len(contents) == 1
